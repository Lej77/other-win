'use strict';

const menuContexts = ['link', 'tab'];


browser.contextMenus.onClicked.addListener(function(onClickData) {
    otherWinUtil.getOtherWindows(function (data) {
        const isUrlLinkClick = !!onClickData.linkUrl;
        const isTabClick = !isUrlLinkClick;

        // if they just clicked on the main option (no window id), just pick the first
        let otherWindowId = data.otherWindows[0].id;
        let possibleWindowId = onClickData.menuItemId.split('-');
        if (possibleWindowId.length === 3) {
            otherWindowId = parseInt(possibleWindowId[2]);
        }

        const otherWindow = data.otherWindows.filter((win) => {
            return win.id = otherWindowId;
        })[0];

        const tabLength = otherWindow.tabs.length;

        if (isUrlLinkClick) {
            openLinkInOtherWindow(onClickData.linkUrl, tabLength, otherWindowId);
        } else if (isTabClick) {
            otherWinUtil.moveTab(otherWindow, data.currentTab);
        }
    });
});

function openLinkInOtherWindow(linkUrl, tabLength, otherWindowId) {
    browser.tabs.create({
        active: true,
        index: tabLength,
        url: linkUrl,
        windowId: otherWindowId
    }).then(() => {
        otherWinUtil.getOptions().then((opts) => {
            if (opts.contextMenuClickBringsFocus) {
                browser.windows.update(otherWindowId, {focused: true});
            }
        });
    });
}

function createMenuItem({name, window, position}) {
    let windowName = name || 'Window ' + position;
    browser.contextMenus.create({
        id: 'other-win-' + window.id,
        title: 'Send to ' + windowName,
        type: 'normal',
        contexts: menuContexts
    });
}

// Just create a menu item without giving window numbers
function createOpenLinkInFirstWindow () {
    browser.contextMenus.create({
        id: 'other-win', // no window id, dynamically choose
        title: 'Send to other window',
        type: 'normal',
        contexts: menuContexts
    });
}

function createAllContextMenus() {
    browser.contextMenus.removeAll();

    otherWinUtil.getAllWindows().then(windows => {
        if (windows.length == 1) {
            return;
        } else if (windows.length == 2) {
            createOpenLinkInFirstWindow();
        } else {
            let orderedWindows = otherWinUtil.sortWindows(windows).then((wins) => {
                wins.forEach((win) => {
                    createMenuItem(win);
                });
            });
        }
    });
}


function setWindowTitlePrefix({name, window, position}) {
    const windowName = name || 'Window ' + position;
    const prefix = windowName + ' - ';
    browser.windows.update(window.id, {titlePreface: prefix});
}

function clearWindowTitlePrefixes() {
    otherWinUtil.getAllWindows().then(allWindows => {
        allWindows.forEach(window => {
            browser.windows.update(window.id, {titlePreface: ''});
        });
    });
}

function setWindowTitlePrefixes() {
    otherWinUtil.getOptions().then(opts => {
        if (!opts.addWindowTitlePrefix) {
            return;
        }
        otherWinUtil.getAllWindows().then(allWindows => {
            otherWinUtil.sortWindows(allWindows).then(windows => {
                windows.forEach(windowVals => {
                    setWindowTitlePrefix(windowVals);
                });
            });
        });
    });
}

function createInitialContextMenus() {
    createAllContextMenus();
    // just recreate the whole gosh darn thing every time
    browser.windows.onCreated.addListener(createAllContextMenus);
    browser.windows.onRemoved.addListener(createAllContextMenus);
}

function recreateInitialContextMenus() {
    otherWinUtil.getOptions().then((opts) => {
        browser.contextMenus.removeAll();
        browser.windows.onCreated.removeListener(createAllContextMenus);
        browser.windows.onRemoved.removeListener(createAllContextMenus);

        if (!opts.hideContextMenu) {
            createInitialContextMenus();
        }
    });
}

browser.storage.local.get('hideContextMenu').then((res) => {
    if (res.hideContextMenu) {
        // we're done here.
        return;
    }

    createInitialContextMenus();
});

// Options menu changed
browser.runtime.onMessage.addListener((request) => {
    if (request.recreateWindowSettings) {
        recreateInitialContextMenus();
        setWindowTitlePrefixes();
    } else if (request.clearAllWindowNames) {
        otherWinUtil.clearAllWindowNames();
        setWindowTitlePrefixes();
    } else if (request.clearWindowPrefixes) {
        clearWindowTitlePrefixes();
    }
});

browser.windows.onCreated.addListener(setWindowTitlePrefixes);
browser.windows.onRemoved.addListener(setWindowTitlePrefixes);
