let otherWinUtil = {};

(function () {
    'use strict';

    const windowNameKey = 'windowName';

    function moveTab(otherWindow, currentTab, callback) {
        let moveProps = {
            windowId: otherWindow.id,
            index: -1
        };

        return browser.tabs.move(
            currentTab.id,
            moveProps
        ).then(() => {
            let updateProps = {
                active: true
            };
            return browser.tabs.update(
                currentTab.id,
                updateProps
            );
        }).then(() => {
            if (callback) {
                return callback();
            }
        });
    }

    function getAllWindows() {
        return browser.windows.getAll({
            windowTypes: ['normal'],
            populate: true
        });
    }

    function getOtherWindows(windowsRetrievedCallback) {

        let windowsP = getAllWindows();

        windowsP.then((windows) => {

            let otherWindows = windows.filter((win) => {
                return !win.focused;
            });

            // there has to be a current window and tab...
            let currWindow = windows.filter((win) => {
                return win.focused;
            })[0];

            if (currWindow === undefined) {
                currWindow = null;
            }

            let currentTab = null;
            if (currWindow !== null) {
                currentTab = currWindow.tabs.filter((tab) => {
                    return tab.active;
                })[0];
            }

            windowsRetrievedCallback({
                otherWindows: otherWindows,
                currentWindow: currWindow,
                currentTab: currentTab
            });
        });
    }

    function getWindowName(window) {
        return browser.sessions.getWindowValue(window.id, windowNameKey);
    }

    function setWindowName(window, windowNameValue) {
        return browser.sessions.setWindowValue(window.id, windowNameKey, windowNameValue);
    }

    function clearAllWindowNames() {
        browser.windows.getAll({
            windowTypes: ['normal'],
            populate: true
        }).then((windows) => {
            windows.map((window) => {
                browser.sessions.removeWindowValue(window.id, windowNameKey);
            });
        });
    }

    /**
     * Sort windows in a set order, named windows first (alphanumeric), then unnamed windows
     * in the order in which they were created.
     * returns a promise that resolves to the array of {name, window, position}, sorted by name
     *   alphabetically, then by position
     */
    function sortWindows(windows) {
        let windowsSortedById = windows.sort((x, y) => x.id - y.id).map((win, idx) => {
            return {
                position: idx + 1,
                window: win
            };
        });

        let promises = windowsSortedById.map((win) => {
            return getWindowName(win.window).then((name) => {
                // if it has a name, prefix zero so it sorts before unnamed windows.
                // Slice is for leftpad.
                return {
                    name: name,
                    // ensure ones with names come before ones without
                    sortVal: name ? ' ' + name : ('00000' + win.id).slice(-5),
                    window: win.window,
                    position: win.position
                };
            });
        });

        return Promise.all(promises).then((values) => {
            return values.sort((x, y) => {
                // sort alphanumerically
                return x.sortVal.localeCompare(y.sortVal);
            });
        });
    }

    /* I'd rather not have to duplicate this from options.js */
    function getOptions() {
        return browser.storage.local.get({
            hideContextMenu: false,
            swapLeftMiddleClick: false,
            contextMenuClickBringsFocus: false,
            addWindowTitlePrefix: false
        });
    }

    otherWinUtil = {
        getOtherWindows: getOtherWindows,
        getAllWindows: getAllWindows,
        moveTab: moveTab,
        getWindowName: getWindowName,
        setWindowName: setWindowName,
        sortWindows: sortWindows,
        clearAllWindowNames: clearAllWindowNames,
        getOptions: getOptions
    };
})();
