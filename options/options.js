function getChecked(id) {
    return document.querySelector('#' + id).checked;
}

function setChecked(id, bool) {
    document.querySelector('#' + id).checked = !!bool;
}

function getOptions() {
    return browser.storage.local.get({
        hideContextMenu: false,
        swapLeftMiddleClick: false,
        contextMenuClickBringsFocus: false,
        addWindowTitlePrefix: false
    });
}

function saveOptions() {
    getOptions().then(opts => {
        const addPrefix = getChecked('addWindowTitlePrefix');
        browser.storage.local.set({
            hideContextMenu: getChecked("hideContextMenu"),
            swapLeftMiddleClick: getChecked("swapLeftMiddleClick"),
            contextMenuClickBringsFocus: getChecked('contextMenuClickBringsFocus'),
            addWindowTitlePrefix: addPrefix
        });

        const addPrefixChanged = addPrefix !== opts.addWindowTitlePrefix;
        if (addPrefixChanged && !addPrefix) {
            browser.runtime.sendMessage({clearWindowPrefixes: true});
        }
    }).then(_ => {
        browser.runtime.sendMessage({recreateWindowSettings: true});
    });;
}

function restoreOptions() {
    getOptions().then((res) => {
        setChecked("hideContextMenu", res.hideContextMenu);
        setChecked('swapLeftMiddleClick', res.swapLeftMiddleClick);
        setChecked('contextMenuClickBringsFocus', res.contextMenuClickBringsFocus);
        setChecked('addWindowTitlePrefix', res.addWindowTitlePrefix);
    });
}

document.getElementById('clearAllWindowNames').addEventListener('click', () => {
    browser.runtime.sendMessage({clearAllWindowNames: true});
});

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("change", saveOptions);
